import sys
import pandas as pd

from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier

TEST_SIZE = 0.4


def main():

    # Check command-line arguments
    if len(sys.argv) != 2:
        sys.exit("Usage: python shopping.py data")

    # Load data from spreadsheet and split into train and test sets
    evidence, labels = load_data(sys.argv[1])
    X_train, X_test, y_train, y_test = train_test_split(
        evidence, labels, test_size=TEST_SIZE
    )

    # Train model and make predictions
    model = train_model(X_train, y_train)
    predictions = model.predict(X_test)
    sensitivity, specificity = evaluate(y_test, predictions)

    # Print results
    print(f"Correct: {(y_test == predictions).sum()}")
    print(f"Incorrect: {(y_test != predictions).sum()}")
    print(f"True Positive Rate: {100 * sensitivity:.2f}%")
    print(f"True Negative Rate: {100 * specificity:.2f}%")


def get_simple_conversion_fields_type():
    """
    Return a dict where the keys are int and float
    and the values are lists of strings represeting which columns
    of data should be simply converted to those types.
    """
    types = {
        int: [
            "Administrative",
            "Informational",
            "ProductRelated",
            "OperatingSystems",
            "Browser",
            "Region",
            "TrafficType"
        ],

        float: [
            "Administrative_Duration",
            "Informational_Duration",
            "ProductRelated_Duration",
            "BounceRates",
            "ExitRates",
            "PageValues",
            "SpecialDay"
        ]
    }

    return types


def map_month_string_to_int(month_string):
    """
    Return the integer corresponding to the month
    string (3 first characters of month name).
    """
    month_map = {
        "JAN": 1,
        "FEB": 2,
        "MAR": 3,
        "APR": 4,
        "MAY": 5,
        "JUN": 6,
        "JUL": 7,
        "AUG": 8,
        "SEP": 9,
        "OCT": 10,
        "NOV": 11,
        "DEC": 12,
    }

    return month_map[month_string.upper()[:3]]


def map_boolean_string_to_int(boolean_string):
    """
    Return 0 for a string 'FALSE' and 1 for a
    string 'TRUE'.
    """
    boolean_map = {
        "FALSE": 0,
        "TRUE": 1
    }

    return boolean_map[boolean_string.upper()]


def map_visitor_type_to_int(visitor_type_string):
    """
    Return 1 if visitor type is 'Returning_Visitor' and 0 otherwise.
    """
    return int(visitor_type_string.lower() == "returning_visitor")


def load_data(filename):
    """
    Load shopping data from a CSV file `filename` and convert into a list of
    evidence lists and a list of labels. Return a tuple (evidence, labels).

    evidence should be a list of lists, where each list contains the
    following values, in order:
        - Administrative, an integer
        - Administrative_Duration, a floating point number
        - Informational, an integer
        - Informational_Duration, a floating point number
        - ProductRelated, an integer
        - ProductRelated_Duration, a floating point number
        - BounceRates, a floating point number
        - ExitRates, a floating point number
        - PageValues, a floating point number
        - SpecialDay, a floating point number
        - Month, an index from 0 (January) to 11 (December)
        - OperatingSystems, an integer
        - Browser, an integer
        - Region, an integer
        - TrafficType, an integer
        - VisitorType, an integer 0 (not returning) or 1 (returning)
        - Weekend, an integer 0 (if false) or 1 (if true)

    labels should be the corresponding list of labels, where each label
    is 1 if Revenue is true, and 0 otherwise.
    """
    data = pd.read_csv(filename, dtype=str)

    # Column names:
    simple_columns = get_simple_conversion_fields_type()
    month = "Month"
    visitor_type = "VisitorType"
    weekend = "Weekend"
    revenue = "Revenue"

    # Simply converted columns:
    for type, columns in simple_columns.items():
        data[columns] = data[columns].astype(type)

    # Month column:
    data[month] = data[month].map(map_month_string_to_int)

    # VisitorType column:
    data[visitor_type] = data[visitor_type].map(map_visitor_type_to_int)

    # Weekend and Revenue columns:
    for column in weekend, revenue:
        data[column] = data[column].map(map_boolean_string_to_int)

    # Split data into evidence and labels:
    labels = list(data[revenue])
    data.drop(columns=revenue, inplace=True)
    evidence = list(map(list, data.itertuples(index=False)))

    return evidence, labels


def train_model(evidence, labels):
    """
    Given a list of evidence lists and a list of labels, return a
    fitted k-nearest neighbor model (k=1) trained on the data.
    """
    model = KNeighborsClassifier(n_neighbors=1)
    model.fit(evidence, labels)
    return model


def evaluate(labels, predictions):
    """
    Given a list of actual labels and a list of predicted labels,
    return a tuple (sensitivity, specificty).

    Assume each label is either a 1 (positive) or 0 (negative).

    `sensitivity` should be a floating-point value from 0 to 1
    representing the "true positive rate": the proportion of
    actual positive labels that were accurately identified.

    `specificity` should be a floating-point value from 0 to 1
    representing the "true negative rate": the proportion of
    actual negative labels that were accurately identified.
    """
    total = len(labels)

    total_positive_labels = sum(labels)
    right_positive_labels = sum(label and prediction for label, prediction in zip(labels, predictions))
    sensitivity = right_positive_labels / total_positive_labels

    total_negative_labels = total - total_positive_labels
    right_negative_labels = sum(not label and not prediction for label, prediction in zip(labels, predictions))
    specificity = right_negative_labels / total_negative_labels

    return sensitivity, specificity


if __name__ == "__main__":
    main()
